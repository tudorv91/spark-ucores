--------------------------
spark-ucores run directory 
--------------------------

- default run directory, cd here to run your applications, demos, workers etc.

- below are several demo usage examples

-----------------------------------------------
run SPARKCL Pi demo on different hardware types 
-----------------------------------------------

Altera FPGAs -> 

../scripts/spark-submit-and-set-env.sh fpga Altera ACC ../spark-1.3.0-bin-hadoop2.4/bin/spark-submit --verbose --class uml.edu.sparkcl.examples.SparkCLPi --master local[4] --conf spark.executor.memory=1G  --jars /home/userX/projects/aparapi-ucores/src/aparapi/com.amd.aparapi/dist/aparapi.jar  /home/userX/projects/spark-ucores/src/target/SparkCLDemoProject-1.0.jar

AMD CPU/GPU ->

../scripts/spark-submit-and-set-env.sh std AMD CPU ../spark-1.3.0-bin-hadoop2.4/bin/spark-submit --verbose --class uml.edu.sparkcl.examples.SparkCLPi --master local[4] --conf spark.executor.memory=1G  --jars /home/userX/projects/aparapi-ucores/src/aparapi/com.amd.aparapi/dist/aparapi.jar  /home/userX/projects/spark-ucores/src/target/SparkCLDemoProject-1.0.jar

../scripts/spark-submit-and-set-env.sh std AMD GPU ../spark-1.3.0-bin-hadoop2.4/bin/spark-submit --verbose --class uml.edu.sparkcl.examples.SparkCLPi --master local[4] --conf spark.executor.memory=1G  --jars /home/userX/projects/aparapi-ucores/src/aparapi/com.amd.aparapi/dist/aparapi.jar  /home/userX/projects/spark-ucores/src/target/SparkCLDemoProject-1.0.jar

Intel CPU ->

../scripts/spark-submit-and-set-env.sh std Intel CPU ../spark-1.3.0-bin-hadoop2.4/bin/spark-submit --verbose --class uml.edu.sparkcl.examples.SparkCLPi --master local[4] --conf spark.executor.memory=1G  --jars /home/userX/projects/aparapi-ucores/src/aparapi/com.amd.aparapi/dist/aparapi.jar  /home/userX/projects/spark-ucores/src/target/SparkCLDemoProject-1.0.jar

