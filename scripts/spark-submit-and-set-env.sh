#!/bin/bash

#
#spark-submit-and-set-env
#

#$1 = fpga/std
#$2 = platform hint
#$3 = device type

if [ "$#" -lt 3 ]
then
 echo "usage is: [opencl implementation -> fpga/std] [platform hint-> AMD/NVIDIA/Altera] [device type -> CPU/GPU/FPGA]"
 exit 1
fi

# unremark this to exit on any script error, helps catch errors early
#set -e

# check if SPARKCL_HOME is set
if [ -z "$SPARKCL_HOME" ]
then 
  # by default set home base folder to working folder is i.e. where script was launched from 
  # this is also the project root folder by default
  #SPARKCL_HOME="$(pwd)"
  SPARKCL_HOME=$(cd `dirname $0`/.. && pwd)
  echo "SPARKCL_HOME was not set, using working folder => '$SPARKCL_HOME'" 
else 
  echo "SPARKCL_HOME was set to '$SPARKCL_HOME'" 
fi

SPARKCL_RUN="$SPARKCL_HOME/run"
SPARKCL_BIN="$SPARKCL_HOME/bin"

# set fpga environment settings file (example file can be found at aparapi-ucores/env/AlteraV14Env)
# example: 
# FPGA_ENV_FILE="[your projects root]/aparapi-ucores/env/AlteraV14Env"
FPGA_ENV_FILE="../environment/aoclRunEnv14"

# set environment according to selected platform
# for platforms that support the The OpenCL ICD extension we do not need to do much
if [ "$1" == 'fpga' ]
then
  OpenclMode=fpga
  echo "Running in fpga mode"
  source "$FPGA_ENV_FILE"
  if [ "$?" != "0" ]; then
    echo "Error setting fpga environment - remember to set the path to your fpga environment file!"
    exit 1
  fi
else
  OpenclMode=std
  echo "Running in std mode (OpenCL ICD mode)"
fi

# add native libraries path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SPARKCL_BIN/dist.$OpenclMode

#CUSTOM_MEM_OPTIONS="-Xms2000M -Xmx4000M"

#export EXECUTOR_JAVA_OPTS="$CUSTOM_MEM_OPTIONS"

export SPARK_JAVA_OPTS="-Dcom.amd.aparapi.platformHint=$2 -Dcom.amd.aparapi.executionMode=$3"

#export SPARK_WORKER_MEMORY="4g"

#export SPARK_EXECUTOR_MEMORY="4g"

export SPARK_WORKER_DIR="$SPARKCL_RUN"

export APARAPI_CL_BIN_FOLDER="$SPARKCL_RUN"

# remove the used vars ($1-$3)
shift 3

# display msg
echo "Running with: "
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
echo "SPARK_JAVA_OPTS=$SPARK_JAVA_OPTS"
echo "SPARK_WORKER_DIR=$SPARK_WORKER_DIR"
echo "APARAPI_CL_BIN_FOLDER=$APARAPI_CL_BIN_FOLDER"

# exec rest of cmd line
$@
